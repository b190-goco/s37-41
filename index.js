/*
	on start up:

	git init
	npm init -y
	npm install express mongoose cors bcrypt jsonwebtoken


*/

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// mongoDB admin
// username: jygsgoco
// password: peanutz11151998
mongoose.connect("mongodb+srv://jygsgoco:peanutz11151998@wdc028-course-booking.ssabc5g.mongodb.net/b190-Course-Booking?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes)

app.listen(process.env.PORT || 4000, () => console.log(`API now online at port: ${process.env.PORT || 4000}`));