const Course = require("../models/Course.js");

/*
	MINI ACTIVITY
	create a new course object using the mongoose model 
	save the new course object to the database

*/


module.exports.addCourse = (isAdmin, reqBody) => {
	console.log(isAdmin);
	console.log(reqBody);
	let message;
	if (isAdmin === true) {
		if (reqBody.name !== null || reqBody.description !== null || reqBody.price !== null) {
			let newCourse = new Course ({
						name: reqBody.name,
						description: reqBody.description,
						price: reqBody.price
					});
		
				return newCourse.save().then((course,error) => {
					if (error) {
						return false
					}
					else {
						message = `You have successfully created ${newCourse.name}!`
						return message
					}
				})
		}
	}
	else {
		message = "You are not allowed to access this function"
		return message
	}
}

module.exports.getAllCourses = () => {
	return Course.find({ }).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}
module.exports.getCourse = (courseId) => {
	console.log(courseId)
	return Course.find({_id: courseId}).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}
	return Course.updateOne({id: reqParams},updatedCourse).then((course,err) => {
		if (err) {
			return false
		}
		else {
			return true
		}
	})
}
// isolated updating 'isActive' from the updating course
// incorporated admin credentials for accessing this function
module.exports.archiveCourse = (isAdmin, reqParams, reqBody) => {
	console.log(isAdmin);
	console.log(reqBody);
	let message;
	if (isAdmin === true) {
		if (reqBody.name !== null || reqBody.description !== null || reqBody.price !== null) {
			let updatedCourse = {
				isActive: reqBody.isActive
			}
			return Course.updateOne({id: reqParams},updatedCourse).then((course,err) => {
				if (err) {
					return false
				}
				else {
					return Course.findOne({id: reqParams})
				}
			})
		}
	}
	else {
		message = "You are not allowed to access this function"
		return message
	}
}

