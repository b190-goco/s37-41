const User = require("../models/User.js");
const Course = require("../models/Course.js")
const auth = require("../auth.js");

const bcrypt = require("bcrypt");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		}
		else {
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
		// isAdmin: false,
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user,error) => {
		if (error) {
			return false
		}
		else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result===null){
			return false;
		}
		else {
			//return true
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}

/*
Create a /details route that will accept the user’s Id to retrieve the details of a user.

Create a getProfile controller method for retrieving the details of the user:

Find the document in the database using the user's ID
     Reassign the password of the returned document to an empty string
     Return the result back to the frontend
     Process a POST request at the /details route using postman to retrieve the details of the user.

Create a git repository named S37-41.

Add another remote link and push to git with the commit message of Add activity code - S38.

Add the link in Boodle.
*/

/*module.exports.getProfile = (reqBody) => {
	return User.findOne({id: reqBody.id}).then(result => {
		if (result === null){
			return false;
		}
		else {
			console.log(result);
			let retrievedUser = {
				firstName: result.firstName,
				lastName: result.lastName,
				email: result.email,
				password: "",
				isAdmin: result.isAdmin,
				mobileNo: result.mobileNo,
				enrollments: result.enrollments
			};
			console.log(retrievedUser);
			return retrievedUser;
		}
	})
}*/
/*
let retrievedUser = {
				firstName: result.firstName,
				lastName: result.lastName,
				email: result.email,
				password: "",
				isAdmin: result.isAdmin,
				mobileNo: result.mobileNo,
				enrollments: result.enrollments
			};
			*/
module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

module.exports.getAdmin = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {
		return result.isAdmin;
	});
};

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findOne({id: data.userId}).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user,error) => {
			if (error) {
				return false
			}
			else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findOne({id: data.courseId}).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,error) => {
			if (error) {
				return false
			}
			else {
				return true
			}
		})
	})
	if (isUserUpdated && isCourseUpdated) {
		return true
	}
	else {
		return false
	}
}