const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const userController = require("../controllers/userController.js")
const auth = require("../auth.js");

router.post("/", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getAdmin({userId: userData.id}).then(resultFromController => 
		courseController.addCourse(resultFromController, req.body).then(resultFromController =>
		res.send(resultFromController)));
})

router.get("/all", (req,res) =>{
	courseController.getAllCourses().then(resultFromController =>
		res.send(resultFromController))
})

module.exports = router;

router.get("/active", (req,res) => {
	courseController.getAllActive().then(resultFromController =>
		res.send(resultFromController));
})
/*
const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
*/

router.get("/:id", (req,res) => {
	courseController.getCourse(req.params.id).then(resultFromController => 
		res.send(resultFromController));
})

router.put("/:id", auth.verify, (req,res) => {
	courseController.updateCourse(req.params,req.body).then(resultFromController =>
		res.send(resultFromController));
})

// isolated the changing of 'isActive' from the update course
// incorporated admin credentials
router.put("/:id/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	courseController.archiveCourse(userData.isAdmin, req.params, req.body).then(resultFromController =>
		res.send(resultFromController));
})
